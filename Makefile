CC = gcc
CFLAGS = -O2 -Wall -I .

# This flag includes the Pthreads library on a Linux box.
# Others systems will probably require something different.


all: cliente servidor

cliente: cliente.c 
	$(CC) $(CFLAGS) -o cliente cliente.c 

servidor: servidor.c
	$(CC) $(CFLAGS) -o servidor servidor.c 


clean:
	rm -f *.o cliente servidor *~

